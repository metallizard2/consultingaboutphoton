﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManager : MonoBehaviour
{
    public GameObject OnlineCanvas;
    public GameObject OfflineCanvas;
    public Text Log;
    public InputField UserID;

    #region Photon Callback
    private void OnJoinedLobby()
    {
        OnlineCanvas.SetActive(true);
        OfflineCanvas.SetActive(false);

        Debug("Joined to lobby");

        AddPlayerPhotonProperties(new ExitGames.Client.Photon.Hashtable() { { "Score", 0 } });
    }

    private void OnCreatedRoom()
    {
        Debug("Create Room " + PhotonNetwork.room.Name);
    }

    private void OnJoinedRoom()
    {
        Debug("Joined Room " + PhotonNetwork.room.Name);

        int prevScore = (int)PhotonNetwork.player.CustomProperties["Score"];
        Debug(prevScore.ToString());
        int score = prevScore + 10;
        AddPlayerPhotonProperties(new ExitGames.Client.Photon.Hashtable() { { "Score", score } });
    }

    private void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        Debug("OnPhotonPlayerConnected. Score is : " + newPlayer.CustomProperties["Score"].ToString());
    }

    private void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        OnlineCanvas.SetActive(true);
        OfflineCanvas.SetActive(false);

        Debug("OnPhotonCustomRoomPropertiesChanged");
        foreach (var key in propertiesThatChanged.Keys)
        {
            Debug(key.ToString());
        }
        foreach (var value in propertiesThatChanged.Values)
        {
            Debug(value.ToString());
        }
    }

    private void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Debug(codeAndMsg[0].ToString() + " " + codeAndMsg[1].ToString());

        switch ((short)codeAndMsg[0])
        {
            case 32749:
                Reconnect();
                break;
        }
    }

    private void OnReceivedRoomListUpdate()
    {
        Debug("RoomList Updated");

        // Fetch rooms
        var rooms = PhotonNetwork.GetRoomList();

        if (rooms.Length < 1)
        {
            Debug("No Room !");
        }
        else
        {
            foreach (var room in rooms)
            {
                Debug(room.Name);
            }
        }
    }

    private void OnDisconnectedFromPhoton()
    {
        OnlineCanvas.SetActive(false);
        OfflineCanvas.SetActive(true);
}
    #endregion

    public void ConnectToServer()
    {
        PhotonNetwork.AuthValues = new AuthenticationValues(UserID.text);
        PhotonNetwork.autoJoinLobby = true;
        PhotonNetwork.ConnectUsingSettings("0.1a");

        OfflineCanvas.SetActive(false);
    }

    public void CreateRoom()
    {
        RoomOptions options = new RoomOptions();
        options.EmptyRoomTtl = 0;  // Immediately close room when empty.
        options.PlayerTtl = 60000; // 60 seconds.
        options.MaxPlayers = 3;
        PhotonNetwork.CreateRoom("MyRoom", options, null);
    }

    public void JoinRoom()
    {   
        PhotonNetwork.JoinRoom("MyRoom");
    }

    public void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }

    public void RejoinRoom()
    {
        PhotonNetwork.ReJoinRoom("MyRoom");
    }

    public void Reconnect()
    {
        // I want to reconnect to my room when the game is quitting because of unexpected quit (Game crash, Internet down etc) what should I do ?
        Debug("Starting Reconnect Procedure");
        PhotonNetwork.ReconnectAndRejoin();
    }

    private void AddPlayerPhotonProperties(ExitGames.Client.Photon.Hashtable hashtable)
    {
        PhotonNetwork.player.SetCustomProperties(hashtable);
    }

    private void Debug(string info)
    {
        print(info);
        Log.text += "\n" + info;
    }
}
